#include"RK.h"

int hash_begin(int strLen)  //����������� ��������� h
{
	int a = 2;  //���������

	int h = 1; 
	for (int i = 1; i < strLen; i++) 
	{
		h = (h * a);
	}
	return h;
}

int ringHash(char* str, unsigned int strLen, int prevHash, int *h) 
{
	int a = 2; //��������� �� �������

					  //h = a^(len-1) - ��������� ��� ��������� ��������� ����
	if (*h == 0) { //���� ��������� �� ����������������
		*h = hash_begin(strLen);
	}

	//���� ������� ��� ������ ���
	if (prevHash == 0) {
		for (int i = 0; i<strLen; i++) 
		{
			prevHash = (a*prevHash + (int)str[i]);  //��������� �� �������
		}
		return prevHash;
	}
	//���� ������������� ��� ������� ����
	else 
	{
		//� ����������� ���� ��������� ��������� ������� � �������� ������ 
		int hash = (a * (prevHash - (int)str[0] * (*h)) + (int)str[strLen]); 
		
		return hash;
	}
}

int rabinKarp(char* text, char* str)
{
	int strLen = strlen(str);
	int textLen = strlen(text);
	int h = 0; //h = a ^ (len - 1)

	//��� ��������� ��� ������
	int strHash = ringHash(str, strLen, 0, &h);
	//��� ������� ���� � ������
	int textHash = ringHash(text, strLen, 0, &h);

	for (int k = 0; k <= (textLen - strLen); k++)
	{
		if (strHash == textHash) 
		{
			//���� ���� �������, ��������� �����������
			for (int i = 0; (i < strLen) && (str[i] == text[k + i]); i++) 
			{
				if (i == (strLen - 1)) 
				{
					cout << k;
					return k;
				}
			}
		}

		//��� ���������� ����
		textHash = ringHash(&text[k], strLen, textHash, &h);
	}

	//������ �� �������
	return -1;
}